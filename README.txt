DerelictLua by Michael Weber, Jr.

This project is bindings to the Lua programming language (www.lua.org) using the Derelict2 
project (www.dsource.org/projects/derelict). This project only supports Lua 5.2 and a D2 compiler.

Currently this project only targets windows. If someone comes up with code for other
operating systems, I will be glad to include it.

To build on windows using the build.bat you must check out DerelictLua into the same
folder as your Derelict2 location. The DerelictLua folder should be in the same directory
as DerelictUtil, DerelictGL, etc... Otherwise you need to edit the build.bat file to
reflect your directory setup. By default it will create DerelictLua.lib in the \lib folder
and it will create interface files in the \import folder.

DerelictLua comes with four import files:
derelict\lua\lua.d : Imports luatypes.d and luafuncs.d and defines the DerelictLua import object
derelict\lua\luatypes.d : Various enums and aliases to the Lua types
derelict\lua\luafuncs.d : The Lua functions
derelict\lua\luad.d : Templetes to common Lua functions for D

WARNING: In building DerelictLua I found that compiling with -O caused some very bad things
to happen that I think are caused by a compiler error but I am looking into. For the mean
time do not compile DerelictLua or any code that links to it with -O.

derelict\lua\luad.d must imported on its own and needs some explaining. 
The other bindings in Derelict do very little to clean up the interface between C and D 
by design. For this project I felt that Lua has a number functions that all do the same 
basic thing but for different types and would benefit from the ability to group them into 
one function to call. In that light luad.d defines the following functions: 
These functions all evalutate their parameters at compile time

bool lua_is(T)(lua_State*, int)
This function overloads the lua_is*(lua_State*, int) functions.
e.g. lua_is!lua_Number(lua_state, -1); // checks the top of the stack to see if it is a lua_Number

T lua_to(T)(lua_State*, int)
This function overloads the lua_to*(lua_State*, int) functions.
e.g. auto x = lua_to!int(lua_state, -1); // returns the top of the stack as an int

void lua_push(T)(lua_State*, T)
This function overloads the lua_push*(lua_State*, int) functions.
e.g. lua_push!int(lua_state, 20);
Only lua_tostring is supporeted from the string facilities as lua_push!string
Some of the push functions return pointers to the data, this function discards them

void lua_push(T)(lua_State*)
Overload of the previous function for lua_pushthread and lua_pushnil

void lua_push(T)(lua_State*, T, int)
Overload of the previous function for lua_pushcclosure

In order to support the above functions, luad.d defines the following aliases:
alias lua_State* lua_Thread; // Note that this is already a pointer type and that "lua_Thread* thread" is wrong
alias void* lua_Function;
alias void* lua_Table;
alias void* lua_Userdata;
alias void* lua_LightUserdata;
alias void  lua_Nil;

I am pretty sure everything works but I have not tested it throughly, please let me know
of any bugs that you discover. But while we are on that subject, luaL_loadfile does not
work for me and I have not found out why. A workaround is to load the file in D, build a
string and use luaL_dostring instead.
