dmd -H -c derelict\lua\lua derelict\lua\luafuncs derelict\lua\luatypes derelict\lua\luad -I..\import
dmd -lib -inline -release -of..\lib\DerelictLua.lib derelict\lua\lua derelict\lua\luatypes derelict\lua\luafuncs derelict\lua\luad ..\lib\DerelictUtil.lib -I..\import
mkdir ..\import\derelict\lua
copy /Y lua.di ..\import\derelict\lua\
copy /Y luatypes.di ..\import\derelict\lua
copy /Y luafuncs.di ..\import\derelict\lua
copy /Y luad.di ..\import\derelict\lua
del /Q obj lua.di luatypes.di luafuncs.di luad.di lua.obj luatypes.obj luafuncs.obj luad.obj